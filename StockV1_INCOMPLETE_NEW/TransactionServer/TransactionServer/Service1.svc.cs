﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
//needed for SQL access
using System.Data.SqlClient;

namespace TransactionServer
{
    public class Service1 : IStockExchange
    {
        private String connStr = "Data Source=localhost;Initial Catalog=TransactionServer;Integrated Security=True";

        // -- DO NOT touch the getPrice() implementation. WILL BE IMPROVED in revisions later.
        public RESULT_PRICE getPrice(String stock_id)
        {
            RESULT_PRICE result = new RESULT_PRICE();
            result.OK = true;
            result.MESSAGE = "OK";
            result.PRICE_CENTS = 200;
            result.TIMESTAMP = DateTime.Now;
            return result;
        }
       
        //Task1 (User Story1): Register Account. Check Requirements in IService1.cs
        public RESULT_ACCOUNT RegisterAccount(String uname, String fname, String lname, String password, long initialBalance_100)
        {            
            RESULT_ACCOUNT res = new RESULT_ACCOUNT();            
            //1. try to establish connection, and begin the transaction
            
            //2. perform SQL selection, check existence of UNAME
                                
            //3. perform insertion               

            //4. read out the new account id                
            
            return res;
        }

        public RESULT_PRICE buy(int account_id, String password, String stock_id, int amount)
        {
            RESULT_PRICE res = new RESULT_PRICE();
            //1. try to establish connection, and begin the transaction
            SqlConnection conn = new SqlConnection(this.connStr);
            conn.Open();
            SqlTransaction trans = conn.BeginTransaction(System.Data.IsolationLevel.Serializable);
            try
            {
                //2. check validity of password, and retrieve the balance of the account
                String str2 = "SELECT BALANCE_CENTS FROM TBL_ACCOUNT WHERE ID=@id AND PASSWORD=@pwd";
                SqlCommand cmd2 = new SqlCommand(str2, conn, trans);
                cmd2.Parameters.AddWithValue("@id", account_id);
                cmd2.Parameters.AddWithValue("@pwd", password);
                SqlDataReader reader = cmd2.ExecuteReader();
                if (!reader.HasRows) //Password is incorrect
                {
                    reader.Close();
                    throw new Exception("Password incorrect");
                }//2 if
                reader.Read();
                Int64 balance_cents = reader.GetInt64(0);

                //3. get the price of the stock, call getPrice(stock_id)
                RESULT_PRICE price = this.getPrice(stock_id);
                Int64 remaining_balance = balance_cents - amount * price.PRICE_CENTS - 200;

                reader.Close();

                if (remaining_balance < 0) //There isn't enough money to buy shares
                {
                    throw new Exception("You don't have sufficient funds!");
                }//3 if

                //4. update the balance of the account
                String str4 = "UPDATE TBL_ACCOUNT SET BALANCE_CENTS = @newbal WHERE ID=@id";
                SqlCommand cmd4 = new SqlCommand(str4, conn, trans);
                cmd4.Parameters.AddWithValue("@newbal", remaining_balance);
                cmd4.Parameters.AddWithValue("@id", account_id);
                cmd4.ExecuteNonQuery();

                //5. update the ownership table, two cases: UPDATE and INSERT
                String str5 = "SELECT AMOUNT FROM TBL_OWNERSHIP WHERE ACCOUNT_ID=@id AND STOCK_ID=@sid";

                SqlCommand cmd5 = new SqlCommand(str5, conn, trans);
                cmd5.Parameters.AddWithValue("@id", account_id);
                cmd5.Parameters.AddWithValue("@sid", stock_id);
                SqlDataReader reader5 = cmd5.ExecuteReader();


                if (!reader5.HasRows) //the user does not own any shares of the stock so there wouldn't be any rows to show ownership, so we will insert for the purchase
                {
                    reader5.Close();
                    String in5str = "INSERT INTO TBL_OWNERSHIP (ACCOUNT_ID, STOCK_ID, AMOUNT) VALUES (@id, @sid, @amt)";
                    SqlCommand cmd5in = new SqlCommand(in5str, conn, trans);
                    cmd5in.Parameters.AddWithValue("@id", account_id);
                    cmd5in.Parameters.AddWithValue("@sid", stock_id);
                    cmd5in.Parameters.AddWithValue("@amt", amount);
                    cmd5in.ExecuteNonQuery();
                }//5 if
                else //the user already owns some shares of the specific stock, use UPDATE
                {
                    reader5.Read();
                    Int32 stock_amount = reader5.GetInt32(0);
                    reader5.Close();
                    Int32 new_amount = amount + stock_amount;

                    String up5str = "UPDATE TBL_OWNERSHIP SET AMOUNT=@amt WHERE ACCOUNT_ID=@id AND STOCK_ID=@sid";
                    SqlCommand cmd5up = new SqlCommand(up5str, conn, trans);
                    cmd5up.Parameters.AddWithValue("@amt", new_amount);
                    cmd5up.Parameters.AddWithValue("@id", account_id);
                    cmd5up.Parameters.AddWithValue("@sid", stock_id);
                    cmd5up.ExecuteNonQuery();
                }//5 else

                //6. insert into  the transactions table

                String transstr = "INSERT INTO TBL_TRANSACTIONS (ACCOUNT_ID, STOCK_ID, AMOUNT, TYPE, PRICE_CENTS, FEE_CENTS, TIMESTAMP) VALUES (@id, @sid, @amt, @typestr, @price, @fee, @time)";
                SqlCommand cmdtrans = new SqlCommand(transstr, conn, trans);

                cmdtrans.Parameters.AddWithValue("@id", account_id);
                cmdtrans.Parameters.AddWithValue("@sid", stock_id);
                cmdtrans.Parameters.AddWithValue("@amt", amount);
                cmdtrans.Parameters.AddWithValue("@typestr", "BUY");
                cmdtrans.Parameters.AddWithValue("@price", price.PRICE_CENTS);
                cmdtrans.Parameters.AddWithValue("@fee", 200);
                cmdtrans.Parameters.AddWithValue("@time", DateTime.Now);
                cmdtrans.ExecuteNonQuery();

                //7. commit transaction and return                       

                res.OK = true;
                res.MESSAGE = "Buy transaction completed!";
                res.PRICE_CENTS = price.PRICE_CENTS;
                res.STOCK_ID = stock_id;
                res.TIMESTAMP = price.TIMESTAMP;
                trans.Commit();
            }//try
            catch (Exception exc)
            {
                res.OK = false;
                res.MESSAGE = exc.Message;
                res.STOCK_ID = stock_id;
                res.PRICE_CENTS = -1;
                res.TIMESTAMP = DateTime.Now;
                trans.Rollback();
            }//catch
            finally
            {
                conn.Close();
            }//finally 
            return res;
        }

        //Task3 (User Story3): Sell. Check Requirements in IService1.cs
        public RESULT_PRICE sell(int account_id, String password, String stock_id, int amount)
        {
            //1. try to establish connection, and begin the transaction
            RESULT_PRICE res2 = new RESULT_PRICE();
            SqlConnection conn2 = new SqlConnection(this.connStr);
            conn2.Open();
            SqlTransaction trans = conn2.BeginTransaction(System.Data.IsolationLevel.Serializable);

            try
            {
                //2. check validity of password, and retrieve the balance of the account                
                String str2 = "SELECT BALANCE_CENTS FROM TBL_ACCOUNT WHERE ID=@id AND PASSWORD=@pwd";
                SqlCommand cmd2 = new SqlCommand(str2, conn2, trans);
                cmd2.Parameters.AddWithValue("@id", account_id);
                cmd2.Parameters.AddWithValue("@pwd", password);
                SqlDataReader reader2 = cmd2.ExecuteReader();
                if (!reader2.HasRows) //Password is incorrect
                {
                    reader2.Close();
                    throw new Exception("Password incorrect");
                }//2 if
                reader2.Read();
                Int64 balance_cents = reader2.GetInt64(0);

                //3. get the price of the stock, call getPrice(stock_id)
                RESULT_PRICE price = this.getPrice(stock_id);
                Int64 new_balance = balance_cents + amount * price.PRICE_CENTS - 200;
                reader2.Close();

                //4. check/update the ownership table. If the owner does not have so many to sell, throw exception


                String str4ch = "SELECT AMOUNT FROM TBL_OWNERSHIP WHERE ACCOUNT_ID=@id AND STOCK_ID=@sid";
                SqlCommand cmd4ch = new SqlCommand(str4ch, conn2, trans);
                cmd4ch.Parameters.AddWithValue("@id", account_id);
                cmd4ch.Parameters.AddWithValue("@sid", stock_id);
                SqlDataReader reader4 = cmd4ch.ExecuteReader();

                if (!reader4.HasRows) //the user does not own any shares of the stock so there wouldn't be any rows to show ownership
                {
                    reader4.Close();
                    throw new Exception("No shares to sell");
                }

                reader4.Read();


                Int32 stock_amount = reader4.GetInt32(0);
                Int32 new_amount = stock_amount - amount;
                if (new_amount < 0) //There are not enough shares to sell -> Exception
                {
                    reader4.Close();
                    throw new Exception("Not enough shares to sell");
                }//4 if 
                else //There are enough shares to sell
                {
                    reader4.Close();
                    String str4up = "UPDATE TBL_OWNERSHIP SET AMOUNT=@amt WHERE ACCOUNT_ID=@id AND STOCK_ID=@sid";
                    SqlCommand cmdup = new SqlCommand(str4up, conn2, trans);
                    cmdup.Parameters.AddWithValue("@amt", new_amount);
                    cmdup.Parameters.AddWithValue("@id", account_id);
                    cmdup.Parameters.AddWithValue("@sid", stock_id);
                    cmdup.ExecuteNonQuery();
                }//4 else

                //5. update the balance of the account
                String str5 = "UPDATE TBL_ACCOUNT SET BALANCE_CENTS = @newbal WHERE ID=@id";
                SqlCommand cmd5 = new SqlCommand(str5, conn2, trans);
                cmd5.Parameters.AddWithValue("@newbal", new_balance);
                cmd5.Parameters.AddWithValue("@id", account_id);
                cmd5.ExecuteNonQuery();

                //6. insert into  the transactions table


                String transstr = "INSERT INTO TBL_TRANSACTIONS (ACCOUNT_ID, STOCK_ID, AMOUNT, TYPE, PRICE_CENTS, FEE_CENTS, TIMESTAMP) VALUES (@id, @sid, @amt, @typestr, @price, @fee, @time)";
                SqlCommand cmdtrans = new SqlCommand(transstr, conn2, trans);

                cmdtrans.Parameters.AddWithValue("@id", account_id);
                cmdtrans.Parameters.AddWithValue("@sid", stock_id);
                cmdtrans.Parameters.AddWithValue("@amt", amount);
                cmdtrans.Parameters.AddWithValue("@typestr", "SELL");
                cmdtrans.Parameters.AddWithValue("@price", price.PRICE_CENTS);
                cmdtrans.Parameters.AddWithValue("@fee", 200);
                cmdtrans.Parameters.AddWithValue("@time", DateTime.Now);
                cmdtrans.ExecuteNonQuery();

                //7. commit transaction and return
                res2.OK = true;
                res2.MESSAGE = "Sell transaction completed!";
                res2.PRICE_CENTS = price.PRICE_CENTS;
                res2.STOCK_ID = stock_id;
                res2.TIMESTAMP = price.TIMESTAMP;
                trans.Commit();
            }//try

            catch (Exception exc)
            {
                res2.OK = false;
                res2.MESSAGE = exc.Message;
                res2.STOCK_ID = stock_id;
                res2.PRICE_CENTS = -1;
                res2.TIMESTAMP = DateTime.Now;
                trans.Rollback();
            }//catch

            finally
            {
                conn2.Close();
            }//finally

            return res2;

        }//sell
    }
}
