﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
//needed for SQL access
using System.Data.SqlClient;

namespace TransactionServer
{
    public class Service1 : IStockExchange
    {
        private String connStr = "Data Source=localhost;Initial Catalog=TransactionServer;Integrated Security=True";

        // -- DO NOT touch the getPrice() implementation. WILL BE IMPROVED in revisions later.
        public RESULT_PRICE getPrice(String stock_id)
        {
            RESULT_PRICE result = new RESULT_PRICE();
            result.OK = true;
            result.MESSAGE = "OK";
            result.PRICE_CENTS = 200;
            result.TIMESTAMP = DateTime.Now;
            return result;
        }
       
        //Task1 (User Story1): Register Account. Check Requirements in IService1.cs
        public RESULT_ACCOUNT RegisterAccount(String uname, String fname, String lname, String password, long initialBalance_100)
        {
            RESULT_ACCOUNT res = new RESULT_ACCOUNT();

            //1. try to establish connection, and begin the transaction
            SqlConnection conn = new SqlConnection(connStr);

            try
            {
                if (uname == null || uname.Length == 0)
                {
                    throw new Exception("UNAME is required,");
                }

                if (fname == null || fname.Length == 0)
                {
                    throw new Exception("FNAME is required,");
                } 
                
                if (lname == null || lname.Length == 0)
                {
                    throw new Exception("LNAME is required,");

                } if (password == null || password.Length == 0)
                {
                    throw new Exception("PASSWORD is required,");
                }

                //2. perform SQL selection, check existence of UNAME
                conn.Open();
                SqlCommand chkUname = new SqlCommand("SELECT UNAME FROM TBL_ACCOUNT WHERE UNAME = @uname", conn);
                chkUname.Parameters.AddWithValue("@uname", uname);
                SqlDataReader reader = chkUname.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Close();
                    throw new Exception("UNAME Already Exists");
                }
                conn.Close();

                //3. perform insertion   
                conn.Open();

                string insertSQL = "INSERT INTO TBL_ACCOUNT (FNAME, LNAME, BALANCE_CENTS, UNAME, PASSWORD) values (";
                insertSQL += "'" + fname + "',";
                insertSQL += "'" + lname + "',";
                insertSQL += initialBalance_100 + ",";
                insertSQL += "'" + uname + "',";
                insertSQL += "'" + password + "')";

                SqlCommand insAccount = new SqlCommand(insertSQL, conn);
                insAccount.ExecuteNonQuery();

                conn.Close();

                //4. read out the new account id   
                conn.Open();
                SqlCommand getId = new SqlCommand("SELECT ID FROM TBL_ACCOUNT WHERE UNAME = @uname", conn);
                getId.Parameters.AddWithValue("@uname", uname);
                reader = getId.ExecuteReader();

                if (!reader.HasRows)
                {
                    reader.Close();
                    throw new Exception("Problem retrieving ID for UNAME of " + uname);
                }

                reader.Read();

                int accountId = reader.GetInt32(0);

                res.OK = true;
                res.MESSAGE = "Register Account transaction completed successfully.";
                res.ACCOUNT_ID = accountId;
            }
            catch(Exception exc)
            {
                res.OK = false;
                res.MESSAGE = exc.Message;
                return res;
            }
            finally
            {
                conn.Close();
            }
      
            return res;
        }



        //Task2 (User Story2): Buy. Check Requirements in IService1.cs
        public RESULT_PRICE buy(int account_id, String password, String stock_id, int amount)
        {
            RESULT_PRICE res = new RESULT_PRICE();
            //1. try to establish connection, and begin the transaction
            
                //2. check validity of password, and retrieve the balance of the account
               
                //3. get the price of the stock, call getPrice(stock_id)
               
                //4. update the balance of the account
                
                //5. update the ownership table, two cases: UPDATE and INSERT
               
                //6. insert into  the transactions table
                
                //7. commit transaction and return                       
                       
            return res;
        }

        //Task3 (User Story3): Sell. Check Requirements in IService1.cs
        public RESULT_PRICE sell(int account_id, String password, String stock_id, int amount)
        {
            //1. try to establish connection, and begin the transaction
            RESULT_PRICE res = new RESULT_PRICE();
            
                //2. check validity of password, and retrieve the balance of the account                

                //3. get the price of the stock, call getPrice(stock_id)
                
                //4. check/update the ownership table. If the owner does not have so many to sell, throw exception
                
                //5. update the balance of the account
                              
                //6. insert into  the transactions table
               
                //7. commit transaction and return
               
            return res;
        }
    }
}
